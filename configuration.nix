# Edit this configuration file to define what should be installed on
#
# programs.
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, lib, pkgs, ... }:


let fzfkak = builtins.fetchGit {
    url = "https://github.com/andreyorst/fzf.kak.git";
    ref = "master";
  };

my-python-packages = python-packages: with python-packages; [
    azure-cosmos
    ];

python-with-my-packages = pkgs.python3.withPackages my-python-packages;


elixir-ls = pkgs.stdenv.mkDerivation {
          name = "elixir-ls";
          nativeBuildInputs = [ pkgs.unzip ];
          buildInputs = [ pkgs.unzip ];

          src = pkgs.fetchurl {
              url = "https://github.com/elixir-lsp/elixir-ls/releases/download/v0.5.0/elixir-ls.zip";
              sha256= "df6914d5c51ceaafddf71f2171e2f784a35e76204197e6d38cedd911de352f01";
           };

         phases ="installPhase";
          installPhase= ''
          mkdir -p $out/bin
          unzip $src -d $out/bin
          ln -s $out/bin/language_server.sh $out/bin/elixir-ls
          '';
       };

kak-lsp-config= pkgs.stdenv.mkDerivation {

          name = "kak-lsp-config";

          src = pkgs.fetchurl {
              url = "https://raw.githubusercontent.com/ul/kak-lsp/master/kak-lsp.toml";
              sha256 = "d316c59731b3379d21a3abe531ae903e1fcaf5b219f33c3b9e4e9cc2e1fed026";
           };

    	  buildInputs = [elixir-ls pkgs.which];
    	  phases="installPhase";


          installPhase =
          ''
          mkdir -p "$out"
          cat $src >> $out/kak-lsp.toml
          printf "\n" >> $out/kak-lsp.toml
          echo "[language.elixir]" >> $out/kak-lsp.toml
          echo 'filetypes = ["elixir"]' >> $out/kak-lsp.toml
          echo 'roots = [".git",  "mix.exs"]' >> $out/kak-lsp.toml
          printf "command = \"$(which elixir-ls)\"" >> $out/kak-lsp.toml
          '';
       };

in
{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      (import "${builtins.fetchTarball https://github.com/rycee/home-manager/archive/master.tar.gz}/nixos")
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.kernelPackages = pkgs.linuxPackages_latest;
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  #
  networking.hostName = "nixos"; # Define your hostname.
  networking.networkmanager.enable = true;
  programs.nm-applet.enable = true;
  hardware.enableRedistributableFirmware = true;

  powerManagement.powertop.enable=true;

    hardware.bluetooth.enable = true;
    services.blueman.enable = false;

  hardware.opengl.driSupport32Bit = true;
  hardware.opengl.extraPackages32 = with pkgs.pkgsi686Linux; [ libva ];
  hardware.pulseaudio.support32Bit = true;

  time.timeZone = "Europe/Amsterdam";

   environment.systemPackages = [
     pkgs.wget
     pkgs.postman
     pkgs.eventstore
     pkgs.inotify-tools
     pkgs.nodejs
     pkgs.gnumake
     pkgs.arandr
     pkgs.xorg.xbacklight
     pkgs.vim
     pkgs.yarn
     pkgs.xclip
     pkgs.steam
     pkgs.xsel
     pkgs.brave
     pkgs.openssl
     pkgs.libffi
     pkgs.networkmanagerapplet
     pkgs.wirelesstools
     pkgs.termite
     pkgs.silver-searcher
     pkgs.ripgrep
     pkgs.slack
     pkgs.kak-lsp
     kak-lsp-config
     python-with-my-packages
     pkgs.elixir
     pkgs.unzip
   ];

   nixpkgs.config.allowUnfree = true;

    fonts.fonts = with pkgs; [
    hermit
    source-code-pro
    terminus_font
    ];


  # Enable sound.
   sound.enable = true;

    hardware.pulseaudio = {
            enable = true;
             package = pkgs.pulseaudioFull;
                };

  # Enable t[he X11 windowing system.
   services.xserver = {
       enable = true;
       libinput.enable = true;

       desktopManager = {
           xterm.enable = false;
       };

       displayManager = {
          defaultSession = "none+i3";
       };

       dpi = 220;

       windowManager.i3 = {
           enable = true;
           extraPackages = with pkgs; [
               dmenu
               i3status
               i3lock
               i3blocks
               ];
         };
      };
  virtualisation.docker.enable = true;

  services.postgresql = {
      enable = false;
       package = pkgs.postgresql_11;
       dataDir = "/data/postgresql";
   };

   services.nginx = {
     enable = true;
     recommendedProxySettings = true;

     virtualHosts."localhost" = {
       locations."/" = {
         proxyPass ="http://localhost:4000";
         proxyWebsockets= true;
       };
       locations.":8080" = {
         proxyPass ="http://localhost:4000";
         proxyWebsockets= true;
       };
       };

       
     virtualHosts."localhost:8080" = {
       locations."/" = {
         proxyPass ="http://localhost:4000";
         proxyWebsockets= true;
       };
       };
      };



    programs.light.enable=true;
       
     



   users.users.ask = {
     isNormalUser = true;
     extraGroups = [ "wheel" "networkmanager" "docker" "video"]; # Enable ‘sudo’ for the user.
     initialHashedPassword = "test";
     shell = pkgs.fish;
   };

 home-manager.users.ask= {
     programs.git = {
         enable = true;
         userName = "Aksel Stadler";
         userEmail = "aksel@stadler.no";
     };


     programs.fish = {
         enable = true;
         shellAliases = {
             tmux = "tmux -2";
             tn = "tmux new-session -s";
             ta = "tmux attach-session -t";
             tl = "tmux list-session";
          };
         loginShellInit= ''
         EDITOR=kak
         '';

      };
      
      programs.fzf = {
          enable = true;
          enableFishIntegration = true;
      };
      

     programs.bash = {
         enable = true;
         shellAliases = {
             tmux = "tmux -2";
             tn = "tmux new-session -s";
             ta = "tmux attach-session -t";
             tl = "tmux list-session";
          };

         initExtra = ''
	if command -v fzf-share >/dev/null; then
  		source "$(fzf-share)/key-bindings.bash"
    		source "$(fzf-share)/completion.bash"
    	fi
         '';
      };


     xsession.windowManager.i3 = {
         enable = true;
         config = {
             terminal = "termite";
             fonts = ["DejaVu Sans Mono, FontAwesome 6"];
             modifier = "Mod4";

             keybindings = lib.mkOptionDefault {
              "Mod4+h" = "focus left";
              "Mod4+j" = "focus down";
              "Mod4+k" = "focus up";
              "Mod4+l" = "focus right";
		};

	     assigns = {
            "1: web" = [{ class = "^Firefox$"; }];
            "5: slack" = [{ class = "Slack"; }];
    };
            };

  	extraConfig = ''
        	for_window [class="floating"] floating enable
        	default_border pixel 1

		bindsym XF86AudioRaiseVolume exec --no-startup-id pactl -- set-sink-volume 0 +5% #increase sound volume
		bindsym XF86AudioLowerVolume exec --no-startup-id pactl -- set-sink-volume 0 -5% #decrease sound volume
		bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute 0 toggle # mute sound

		# Sreen brightness controls
		# bindsym XF86MonBrightnessUp exec xbacklight -inc 20 # increase screen brightness
		# bindsym XF86MonBrightnessDown exec xbacklight -dec 20 # decrease screen brightness
            	'';
     };


      programs.tmux = {
          enable = true;
          keyMode = "vi";
          shortcut = "C-a";
          terminal = "xterm-256color";

          extraConfig = ''

          # Use Alt-vim keys without prefix key to switch panes
          unbind M-l
          bind-key -n M-h select-pane -L
          bind-key -n M-j select-pane -D
          bind-key -n M-k select-pane -U
          bind-key -n M-l select-pane -R
          bind-key l split-window -h
          #
          '';
      };




     programs.kakoune = {
         enable = true;
         config = {
             numberLines = {
                 enable = true;
                 highlightCursor = true;
                 };

             colorScheme = "palenight";
             indentWidth = 2;
             scrollOff.columns = 10;
             scrollOff.lines= 10;

          };





#    	  lsp-enable
          extraConfig =''
          hook global NormalKey y|d|c %{ nop %sh{ printf %s "$kak_main_reg_dquote" | xsel --input --clipboard}}

          map global user p '<a-!>xsel --output --clipboard<ret>' -docstring 'paste from global clipboard'
          hook global InsertChar k %{ try %{
                  exec -draft hH <a-k>jk<ret> d
                    exec <esc>
                    }}
          source "${fzfkak.outPath}/rc/fzf.kak"
          source "${fzfkak.outPath}/rc/modules/fzf-file.kak"
          source "${fzfkak.outPath}/rc/modules/fzf-grep.kak"

	  map global normal <c-p> ': fzf-mode<ret>'
	  map global user f ':fzf-file<ret>' -docstring 'Search for file'
	  map global user a ':fzf-grep<ret>' -docstring 'Search for text'





	  eval %sh{kak-lsp --kakoune --config ${kak-lsp-config}/kak-lsp.toml -s $kak_session}
          hook global WinSetOption filetype=(c|cpp|rust|elixir|ex) %{
            map window user "l" ": enter-user-mode lsp<ret>" -docstring "LSP mode"
            lsp-enable-window
            lsp-auto-hover-enable
            lsp-auto-hover-insert-mode-disable
            set-option window lsp_hover_anchor true
            set-face window DiagnosticError default+u
            set-face window DiagnosticWarning default+u
            }



         '';

  };
};


  system.stateVersion = "20.03";
}


